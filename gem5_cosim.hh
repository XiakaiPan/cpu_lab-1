#ifndef _GEM5_COSIM_H_
#define _GEM5_COSIM_H_

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

// only for debug test
int hello();

// init gem5 environment
int init_gem5(int argc, char **argv);

// run one cycle
int step_gem5();

// ---------------------------------------
// "hijack" a port means the data sent
// from cpu side will be intercepted,
// won't be able to reach mem system
// in gem5, thus we can handle them
//  manually.
void hijack_iport(uint64_t value);
void hijack_dport(uint64_t value);

// instruction fetch request
// for RTL
typedef struct irequest
{
    uint64_t valid;     //
    uint64_t opcode;    //
    uint64_t trans_id;  //
    uint64_t vaddr;     //
    uint64_t paddr;     //
} irequest;


/**
 *  get instruction fetch request
 *  return sturct irequest
 *  if !ready, do nothing
 *  if no request, return witch result.valid==0
 */
irequest l1ic_req_get(uint64_t ready);

// instruction fetch response
// for RTL
typedef struct iresponse
{
    uint64_t valid;
    uint64_t trans_id;  //
    uint8_t  data[32];    //
    uint64_t error;       //
} iresponse;

/**
 * send instruction fetch response 
 * return: "ready"
 * do nothing if ires.valid==0
 * return 0 if not ready
 */
uint64_t l1ic_resp_put(iresponse ires);

// ---------------------------------------
// data interface ---------
// data load/store request
typedef struct drequest
{
    uint64_t valid;
    uint64_t opcode;
    uint64_t trans_id;
    uint64_t vaddr;
    uint64_t paddr;
    uint64_t size;
    uint64_t data;
    uint64_t error;
} drequest;

/**
 * get data request
 * args:
 *     do nothing when ready == 0
 * return:
 *     struct drequest result;
 *     result.valid==0 indicates no request;
 *     result.opcode==0 indicates store; 1 indicate load
 */
drequest l1dc_req_get(uint64_t ready);
uint64_t get_insn_req_addr();
void set_insn_resp(uint64_t);

// set data
int  load_req_exist();
uint64_t get_load_req_addr();
void set_data_resp(uint64_t);

int  store_req_exist();


#ifdef __cplusplus
} // extern "C"
#endif


#endif
